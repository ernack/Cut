#include <cut.h>

CUT_SETUP();

CUT_TEST(test_equal_trivial, {
		CUT_ASSERT(5 == 5);
		CUT_ASSERT(6 > 5);
		CUT_ASSERT(4 < 5);
});

CUT_TEST(test_equal_trivial_2, {
  CUT_ASSERT(5 == 5);
		CUT_ASSERT(2 > 5);
		CUT_ASSERT(4 < 5);
});

CUT_MAIN();
