#ifndef CUT_H
#define CUT_H
#include <stdlib.h>
#include <stdio.h>

#define CUT_SETUP()
typedef struct {\
		char const* name;\
		int result;\
} test_result_t;\
\
typedef void (*test_t)();\
\
size_t cut_test_count = 0; \
test_t cut_tests[256];\
test_result_t result[256]; \
char* cut_name;\
int cut_id = 0; \
int cut_failures; \
\
void cut_add_test(test_t test)\
{\
  cut_tests[cut_test_count] = test;\
		cut_test_count++;		\
}\
\
int cut_run_tests()\
{\
		int success = 0;\
				\
		for (size_t i=0; i<cut_test_count; i++)\
		{\
				cut_failures = 0; \
				cut_id = i;
				cut_tests[i]();\
				success += result[i].result;\
				\
				if (result[i].result == 0)\
				{\
						printf("Test %s failed\n", result[i].name);\
				}\
\
		}\
		\
		return success;\
}\
\
int cut_assert(int expr)\
{\
		return expr != 0;\
}\
int main() {\
		
#define CUT_MAIN() \
	int success = cut_run_tests(); \
		\
  if (success == cut_test_count)\
		{\
				printf("================\n\e[32mAll tests passed : %lu/%lu\e[0m\n", cut_test_count, cut_test_count);\
		}\
		else \
		{\
				printf("================\n\e[31mTest failure : %d/%lu\e[0m\n", success, cut_test_count);\
		}\
	 \
		return 0;\
		}\


#define CUT_TEST(NAME,BODY) \
void NAME()\
{\
		result[cut_id].name = #NAME;\
		int res;\
		BODY\
}\
CUT_REGISTER(NAME);\

#define CUT_ASSERT(EXPR) \
res = cut_assert(EXPR);\
cut_failures += (res == 0) ? 1 : 0;\
result[cut_id].result = (cut_failures == 0) ? 1 : 0;\
if (res == 0) { printf("Failed %s\n", #EXPR); }\

#define CUT_REGISTER(NAME) cut_add_test(&NAME);
		
#endif // CUT_H
